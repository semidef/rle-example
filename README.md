# Run-Length Encoding

Basic Run-Length Encoding project with unit tests. Unit tests are found in the
`tests.cpp` file.

# Building

``` sh
mkdir build && cd build
cmake ../
make
```

## Running Tests

``` sh
ctest 
```
