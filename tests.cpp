#include <gtest/gtest.h>

#include "rle.hpp"

class RleTester : public testing::Test
{
public:
    void TestEncode(const std::string input,
                    const std::string expected_output)
    {
        // Will hold our encoded string.
        std::string output;
        rle.Encode(input, output);
        ASSERT_EQ(output, expected_output);
    }

    void TestDecode(const std::string rle_input,
                    const std::string expected_decode)
    {
        // Will hold our encoded string.
        std::string output;
        rle.Decode(rle_input, output);
        ASSERT_EQ(output, expected_decode);
    }

    /// @brief Perform String (A) -> Encode (B) -> Decode (C) -> Encode (D), and
    /// ensure that both A == C and B == D.
    void TestEncodeDecodeCycle(const std::string input)
    {
        std::string encode_output;
        rle.Encode(input, encode_output);

        std::string decode_output;
        rle.Decode(encode_output, decode_output);

        std::string encode_output_cycle;
        rle.Encode(decode_output, encode_output_cycle);

        ASSERT_EQ(input, decode_output);
        ASSERT_EQ(encode_output, encode_output_cycle);
    }

    /// Helper method to convert a series of Runs to a string.
    /// In this method, a run is a
    /// 1. frequency int the inclusive range [0, 255], and a
    /// 2. Byte value (usually representing an ASCII character)
    std::string RunsToString(std::vector<std::pair<int8_t, uint8_t>> runs)
    {
        std::string output;
        for(const auto& run : runs)
        {
            output.push_back(run.first);
            output.push_back(run.second);
        }
        return output;
    }
    RunLengthEncoder rle;
};

TEST_F(RleTester, TestEncodeEmptyString)
{
    TestEncode("", "");
}

// Essentially "worst case" in terms of space for our RLE.
TEST_F(RleTester, TestShortRunString)
{
    TestEncode("abcd", RunsToString(
                   {{'a', 1},
                    {'b', 1},
                    {'c', 1},
                    {'d', 1} }));
}

TEST_F(RleTester, TestBasicCompressibleString)
{
    TestEncode("aaaaabbbbbbbbbbcccccccccccccccd", RunsToString({
                {'a', 5},
                {'b', 10},
                {'c', 15},
                {'d', 1}
            }));
}

// Ensure 'capping' works with at least two repetitions.
TEST_F(RleTester, TestManyRepeatsInString)
{
    std::string repeat_string;
    repeat_string.append(300, 'a');
    repeat_string.append(600, 'b');

    TestEncode(std::move(repeat_string), RunsToString({
                {'a', rle.kMaxRepeats},
                {'a', 300 % rle.kMaxRepeats},
                {'b', rle.kMaxRepeats},
                {'b', rle.kMaxRepeats},
                {'b', 600 % rle.kMaxRepeats}
            }));
}

TEST_F(RleTester, DecodeEmptyStringTest)
{
    TestDecode("", "");
}

// A string is only "badly formatted" if there are an even number of characters
// in it.
TEST_F(RleTester, DecodeInvalidStringTest)
{
    EXPECT_THROW(TestDecode("a1b", ""), RunLengthEncoder::DecodeException);
}

TEST_F(RleTester, DecodeBasicStringTest)
{
    TestDecode(RunsToString({{'a', 5}, {'b', 5}, {'c', 1}, {'d', 1}}),
               "aaaaabbbbbcd");
}


TEST_F(RleTester, BasicCycleTest)
{
    TestEncodeDecodeCycle("This is a generic string");
    TestEncodeDecodeCycle("Maaaaaaaaaaaannnnnnnnyyyyyyyy Repetitoinssssssssssssssx");
}
