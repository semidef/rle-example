    /// @brief Given a sequence of bytes, performs a run-length encoding of the
    /// input, writing it to the output buffer.
    ///
    /// @param input Null-terminated array of bytes.
    /// @param output_buflen out-parameter that will store the number of bytes
    /// written to the buffer (excluding null-termination). If NULL, nothing is
    /// written to the variable.
    /// @return Dynamically-allocated buffer populated with the run-length
    /// encoded version of the input string. Must be free'd by the caller.
    // char* Encode(const char* input, size_t* output_buflen);
