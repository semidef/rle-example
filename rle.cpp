#include "rle.hpp"

size_t RunLengthEncoder::Encode(const std::string &input, std::string &output) const
{
    output.clear();
    if(input.empty())
    {
        return 0;
    }

    Run current_run = { 1, input[0] };

    const size_t input_len = input.size();
    for(size_t i = 1; i < input_len; i++)
    {
        const int8_t curr_byte = input[i];

        // On a mismatch, encode 'current run' in RLE and create new run.
        if(curr_byte != current_run.byte)
        {
            AppendRun(current_run, output);
            current_run = {0, curr_byte};
        }
        current_run.length++;
    }

    // Write the last run
    AppendRun(current_run, output);
    return output.size();
}

void RunLengthEncoder::AppendRun(const Run& run, std::string& output) const
{
    for(size_t i = 0; i < run.length; i += kMaxRepeats)
    {
        output.push_back(run.byte);

        const size_t next_i = std::min(i + kMaxRepeats, run.length);
        output.push_back(next_i - i);
    }
}

size_t RunLengthEncoder::Decode(const std::string& input, std::string& output) const
{
    output.clear();
    const size_t input_size = input.size();
    size_t input_index = 0;

    Run curr_run;
    while(input_index < input_size)
    {
        ReadRun(curr_run, input, input_index);
        output.append(curr_run.length, curr_run.byte);
        input_index += 2;
    }

    return output.size();
}

void RunLengthEncoder::ReadRun(Run& run, const std::string& rle_string, size_t index) const
{
    if(index + 1 >= rle_string.size())
    {
        throw DecodeException("Could not find 'length' of run at index: ", index);
    }

    // Otherwise, read two bytes and treat it as a run.
    run.byte = rle_string[index];
    run.length = rle_string[index + 1];
}
