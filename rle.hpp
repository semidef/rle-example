#ifndef RLE_HPP
#define RLE_HPP

#include <string>
#include <stdexcept>

/// @brief Manages run-length encoding and decoded strings.
///
/// A Run-Length encoded string (RLE) is a sequence of bytes where a byte
/// can be optionally followed by a number, which represents its frequency. For
/// example, 'a5' represents the string 'aaaaa'
///
/// In this implementation, no optimizations are performed for short runs. In
/// other words, The string "abcd" would be encoded as "a1b1c1d1". In the worst
/// case, the encoded string is twice as long as the input string.
class RunLengthEncoder
{
public:
    RunLengthEncoder() = default;

    /// @brief Given a sequence of bytes, performs a run-length encoding of the
    /// input, writing it to the output buffer.
    ///
    /// @param input Array of bytes. Does not need to be null-terminated.
    /// @param output Array of bytes representing a run-length-encoded version
    /// of the input array.
    /// @return Number of bytes written to the output.
    size_t Encode(const std::string& input, std::string& output) const;

    /// @brief
    class DecodeException : public std::runtime_error
    {
    public:
        DecodeException(const char* reason, size_t offset) :
            std::runtime_error(BuildErrorMessage(offset)) {}
    private:
        std::string BuildErrorMessage(size_t offset) { return ""; }
    };

    /// @brief Given a run-length encoded string, decodes it.
    ///
    /// @param input Array of bytes representing a run-length encoded string.
    /// @param output Decoded version of the run-length-encoded string.
    ///
    /// @return Number of bytes written to the output.
    /// @throws DecodeException if the input is malformed. An input is
    /// considered 'malformed' if there is a character without a subsequent
    /// 'frequency' byte. As an example, the strings "a" and "b\x10c" are both
    /// malformed (since 'a' and 'c' do not have frequencies succeeding them).
    size_t Decode(const std::string& input, std::string& output) const;

    /// @brief Maximum number of repeats for a byte value.
    const size_t kMaxRepeats = 255;
private:
    /// @brief Struct used to represent a "run" (character and a frequency).
    struct Run
    {
        /// @brief Frequency of a character.
        size_t length;

        /// @brief Byte value to encode.
        int8_t byte;
    };

    /// @brief Encodes a run at the end of the specified output string.
    void AppendRun(const Run& run, std::string& output) const;

    /// @brief Given a run-length-encoded string and a position, interpret the
    /// two bytes at the specified index as a (byte, length) pair. If (index) or
    /// (index + 1) is outside the bounds of the input string, a DecodeException
    /// is thrown.
    ///
    /// @param run Run representing a character and a frequency. Out-parameter.
    /// @param rle_string
    void ReadRun(Run& run, const std::string& rle_string, size_t index) const;
};

#endif // RLE_HPP
